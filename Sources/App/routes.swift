import MeerkatServerProvider

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    let helloController = HelloWorldController()
    router.get(use: helloController.index)
}
